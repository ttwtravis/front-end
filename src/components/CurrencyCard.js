import React, { useState, useEffect } from 'react';
import _ from 'lodash';

const CurrencyCard = props => {
  const { data } = props;

  const name = _.get(data, 'name');
  const price = _.get(data, 'response.ticker.price');
  const volume = _.get(data, 'response.ticker.volume');
  const change = _.get(data, 'response.ticker.change');

  const getChangeColor = () => {
    if (change > 0) return '#0c0';
    if (change < 0) return '#c00';
    return;
  };

  return (
    <div
      style={{
        width: 300,
        display: 'inline-block',
        padding: 16,
        margin: 16,
        border: '1px solid #999',
        borderRadius: 8
      }}
    >
      <h1>{name}</h1>
      <h2 style={{ color: '#fc3' }}>{price ? `$${price}` : '-'}</h2>
      <div style={{ color: '#999' }}>
        <div style={{ width: '50%', display: 'inline-block' }}>
          <b>
            volume:
            <br />
          </b>
          {_.round(volume, 8) || '-'}
        </div>
        <div style={{ width: '50%', display: 'inline-block' }}>
          <b>
            change:
            <br />
          </b>
          <span
            style={{
              color: getChangeColor()
            }}
          >
            {_.round(change, 8) || '-'}
          </span>
        </div>
      </div>
    </div>
  );
};

export default CurrencyCard;
