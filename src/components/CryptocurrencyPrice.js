import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CurrencyCard from './CurrencyCard';

import {
  getCurrencies,
  updateCurrenciesAction
} from '../store/currencies/actions';

const CryptocurrencyPrice = props => {
  const dispatch = useDispatch();

  const currencies = useSelector(state => {
    return state.currenciesReducer;
  });

  useEffect(() => {
    const fetchData = async () => {
      const data = await getCurrencies();
      dispatch(updateCurrenciesAction(data));
    };

    fetchData();
  }, []);

  return (
    <div style={{ padding: 40 }}>
      <div style={{ padding: '40px 0' }}>
        <h1>Cryptocurrency Realtime Price</h1>
      </div>
      <div style={{ margin: -16 }}>
        {currencies.map(item => {
          return <CurrencyCard key={item.key} data={item} />;
        })}
      </div>
    </div>
  );
};

export default CryptocurrencyPrice;
