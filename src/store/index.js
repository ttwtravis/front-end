import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import currenciesReducer from './currencies/reducer';

export default createStore(
  combineReducers({
    currenciesReducer
  }),
  applyMiddleware(thunk)
);
