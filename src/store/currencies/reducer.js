const UPDATE = 'UPDATE_CURRENCIES';

const reducer = (state = [], action) => {
  const { type, data } = action;
  switch (type) {
    case UPDATE: {
      return data;
    }
    default:
      return state;
  }
};

export default reducer;
