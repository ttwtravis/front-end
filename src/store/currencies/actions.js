const axios = require('axios');

const UPDATE = 'UPDATE_CURRENCIES';
const BASE_URL = 'http://localhost:3000';

export const getCurrencies = async () => {
  const url = `${BASE_URL}/api/rates`;

  return await axios
    .get(url)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.error(url);
      console.error(error);
      return [];
    });
};

export const updateCurrenciesAction = data => {
  return {
    type: UPDATE,
    data
  };
};
