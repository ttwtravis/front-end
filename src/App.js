import logo from './logo.svg';
import './App.css';
import CryptocurrencyPrice from './components/CryptocurrencyPrice';

function App() {
  return <CryptocurrencyPrice />;
}

export default App;
